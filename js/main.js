$(document).ready(function () {

    /*animation*/
    if($(window).outerWidth() >= 768) {
        $('.anLeft').addClass('hidden').viewportChecker({
            classToAdd: 'visible animated fadeInLeftBig',
            offset: 100
        });
        $('.anRight').addClass('hidden').viewportChecker({
            classToAdd: 'visible animated fadeInRightBig',
            offset: 100
        });
        $('.anUp').addClass('hidden').viewportChecker({
            classToAdd: 'visible animated fadeInUp',
            offset: 100
        });
    }

    /*end animation*/

    /*popup*/
    function fancyPopup() {
        $('.fancyPopup').fancybox({
            padding: 0,
            margin: 15,
            closeBtn: false
        });
        $('.close').on('click',function() {
            $.fancybox.close();
        });
    }
    fancyPopup();
    /*popup*/

    var stickyElement = $('.stickyHead'),
        stickyClass = "sticky",
        stickyPos = stickyElement.offset().top;

    function stickyFn() {
        var winTop = $(this).scrollTop();
        winTop >= stickyPos ?
            stickyElement.addClass(stickyClass):
            stickyElement.removeClass(stickyClass)
    }
    stickyFn();

    $(window).scroll(function() {
        stickyFn();
    });

    var menu_selector = "#phoneMenu";

    function onScroll(){
        var scroll_top = $(document).scrollTop();

        $(menu_selector + " a").each(function(){
            var hash = $(this).attr("href"),
                target = $(hash);
            if (target.position().top <= scroll_top && target.position().top + target.outerHeight() > scroll_top) {
                $(menu_selector + " a.selected").removeClass("selected");
                $(this).addClass("selected");
            } else {
                $(this).removeClass("selected");
            }
        });
    }

    $(document).on("scroll", onScroll);

    $('.AnchorButton').click(function(e){
        e.preventDefault();

        $(document).off("scroll");
        $(menu_selector + " a.selected").removeClass("selected");
        $(this).addClass("selected");
        var hash = $(this).attr("href"),
            target = $(hash);

        $("html, body").animate({scrollTop: target.offset().top}, 500, function(){
            window.location.hash = hash;
            $(document).on("scroll", onScroll);
        });
    });

    var slide1 = $('.slideWrap .slideOne'),
        slide2 = $('.slideWrap .slideTwo');

    if($('.slideWrap').length) {
        $('.slideWrap .slideOne .link').on('click', function () {
            slide1.css({'opacity': '0', 'display': 'none'});
            slide2.css({'opacity': '1', 'display': 'block'})
        });
        $('.slideWrap .slideTwo .link').on('click', function () {
            slide2.css({'opacity': '0', 'display': 'none'});
            slide1.css({'opacity': '1', 'display': 'block'})
        })
    }

    var colorButton = $(".colorSelectors li");
    colorButton.on("click", function(){
        $(".colors > li").removeClass("active");
        $(this).addClass("active");
        var newColor = $(this).attr("data-color");
        $('.colorTheme').attr('data-color', newColor);

    });

    if($(document).find('#btnMenu')){
        $('#btnMenu').on('click', function(){
            $(this).toggleClass('open');
            $(this).parents('.leftWrap').find('ul').slideToggle();
        });
    }

});